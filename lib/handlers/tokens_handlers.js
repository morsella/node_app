/*
* Tokens handlers
*/

// Dependencies
var _data = require('../data');
var helpers = require('../helpers');

var tokens_handlers = {};

// Define Tokesn handler
tokens_handlers.tokens = (data, callback)=>{
  var acceptableMethods = ['post', 'get', 'put', 'delete'];
  if(acceptableMethods.indexOf(data.method) > -1){
    tokens_handlers._tokens[data.method](data, callback);
  }else{
    callback(405);
  }
};

// Container for the tokens submethods
tokens_handlers._tokens = {};

// tokens - Post
// Required data: password, phone, out of payload
// Optional data: none
tokens_handlers._tokens.post = (data, callback)=>{
  // Check that all required fields are filled out
  var password = typeof(data.payload.password) == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false;
  var phone = typeof(data.payload.phone) == 'string' && data.payload.phone.trim().length == 9 ? data.payload.phone.trim() : false;

  if(password && phone){
    // Lookup the User by phone number
    _data.read('users', phone, (err, userData)=>{
      if(!err && userData){
        // Hash sent password and compare it to the password stored in users
        var hashedPassword = helpers.hash(password);
        //Check if password was hashed
        if(hashedPassword == userData.hashedPassword){
            // Create Token with a random name. Set expiration 1 hour in the future
            var tokenId = helpers.createRandomStr(20);
            var expires  = Date.now() + 1000*60*60;

            // Set the token obj
            var tokenObj = {
              'phone': phone,
              'id': tokenId,
              'expires': expires
            };

            // Store token Object
            _data.create('tokens', tokenId, tokenObj, (err, resToken)=>{
              if(!err){
                callback(200, tokenObj);
              }else {
                console.log(err);
                callback(500, {'ERROR': 'Could not create a token'});
              }
            });
        }else{
            callback(400, {'ERROR': 'The passwords do not match'});
        }
      }else{
        // User already exist
        callback(400, {'ERROR': 'Could not find the User with this phone'});
      }
    });
  } else {
    callback(400,{'ERROR' : 'Missing required fields'});
  }
};

// tokens - Get
// Required data: id
// Optional data: none
tokens_handlers._tokens.get = (data, callback)=>{
  // Check that the Id is valid
  var id = typeof(data.queryStringObj.id) == 'string' && data.queryStringObj.id.trim().length == 20 ? data.queryStringObj.id.trim() : false;
  if(id){
    // Lookup the token
    _data.read('tokens',id,(err,tokenData)=>{
      if(!err && tokenData){
        callback(200,tokenData);
      } else {
        callback(404);
      }
    });
  } else {
    callback(400,{'Error' : 'Missing token ID'});
  }
};


// tokens - Put
// Required data: Id, extend, out of payload
// Optional data: none
tokens_handlers._tokens.put = (data, callback)=>{
  // Check ID and expiration date
  var id = typeof(data.payload.id) == 'string' && data.payload.id.trim().length == 20 ? data.payload.id.trim() : false;
  var extend = typeof(data.payload.extend) == 'boolean' && data.payload.extend == true ? true : false;
  if(id && extend){
    //Lookup the Token
    _data.read('tokens', id, (err, tokenData)=>{
      if(!err && tokenData){
       // Check expiration of the token
       if(tokenData.expires > Date.now()){
          // Extend Expiration for another hour
          tokenData.expires = Date.now() + 1000 * 60 * 60;
          // Store the new updated Token
          _data.update('tokens', id, tokenData, (err)=>{
            if(!err){
              callback(200);
            } else {
              callback(500, {'ERROR': 'Could not update the Token expiration'});
            }
          });
       } else{
         callback(400, {'ERROR': 'The token already expired'});
       }
      } else {
        callback(400, {'ERROR': 'Specified token does not exist'});
      }
    });
    } else {
      callback(400, {'ERROR': 'Missing token Id or other required fields'});
    }
};

// tokens - Delete
// Required data: id
tokens_handlers._tokens.delete = (data, callback)=>{
  // Check that the Id is valid
  var id = typeof(data.queryStringObj.id) == 'string' && data.queryStringObj.id.trim().length == 20 ? data.queryStringObj.id.trim() : false;
  if(id){
    //Lookup the token
    _data.read('tokens', id, (err, resData)=>{
      if(!err && resData ){
        // Delete Token
        _data.delete('tokens', id, (err)=>{
          if(!err){
            callback(200);
          } else{
            callback(500, {'ERROR': 'Could not delete the Token'});
          }
        });
      }else{
        callback(400, {'ERROR': 'Could not find the Token with this Id'});
      }
    });
  }else{
      callback(400, {'ERROR' : 'Missing Id number'});
  }
};

// Verify if the given token Id match with the current User
tokens_handlers.verifyToken = (id, phone, callback)=>{
  //Look up the token
  _data.read('tokens', id, (err, tokenData)=>{
    if(!err && tokenData){
      // Check if the token is for the given User and not expired
      if(tokenData.phone == phone && tokenData.expires > Date.now()){
        callback(true);
      } else {
        callback(false);
      }
    } else {
      callback(false);
    }
  });
};


module.exports = tokens_handlers;
