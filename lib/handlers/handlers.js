/*
* Request handlers
*/

// Dependencies
var _data = require('../data');
var helpers = require('../helpers');
var users_handlers = require('./users_handlers');
var tokens_handlers = require('./tokens_handlers');
var checks_handlers = require('./checks_handlers');

//Define handlers
var handlers = {};

//Sample handler
handlers.sample = (data, callback)=>{
  //callback http status code and payload obj
  callback(200, {'name': 'sample handler'});
};
// not found handler
handlers.notFound = (data, callback)=>{
 callback(404);
};
// Define the PING handler
handlers.ping = (data, callback)=>{
  callback(200);
};

handlers.users = users_handlers.users;
handlers.tokens = tokens_handlers.tokens;
handlers.checks = checks_handlers.checks;

module.exports = handlers;
