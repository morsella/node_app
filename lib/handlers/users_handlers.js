/*
* Users handlers
*/

// Dependencies
var _data = require('../data');
var helpers = require('../helpers');
var tokens_handlers = require('./tokens_handlers');
//Define handlers
var users_handlers = {};

// Define the Users handler
users_handlers.users = (data, callback)=>{
  var acceptableMethods = ['post', 'get', 'put', 'delete'];
  if(acceptableMethods.indexOf(data.method) > -1){
    users_handlers._users[data.method](data, callback);
  }else{
    callback(405);
  }
};


// Container for the users submethods
users_handlers._users = {};

// Users - Post
// Required data: firstName, lastName, phone, password, tosAgreement
users_handlers._users.post = (data, callback)=>{
  // Check that all required fields are filled out
  var firstName = typeof(data.payload.firstName) == 'string' && data.payload.firstName.trim().length > 0 ? data.payload.firstName.trim() : false;
  var lastName = typeof(data.payload.lastName) == 'string' && data.payload.lastName.trim().length > 0 ? data.payload.lastName.trim() : false;
  var phone = typeof(data.payload.phone) == 'string' && data.payload.phone.trim().length == 9 ? data.payload.phone.trim() : false;
  var password = typeof(data.payload.password) == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false;
  var tosAgreement = typeof(data.payload.tosAgreement) == 'boolean' && data.payload.tosAgreement == true ? true : false;

  if(firstName && lastName && phone && password && tosAgreement){
    // Make sure that the user doesn't aready exist
    _data.read('users', phone, (err, data)=>{
      if(err){
        // Create user HASH the password
        var hashedPassword = helpers.hash(password);
        //Check if password was hashed
        if(hashedPassword){
            // Create User obj
            var userObj = {
              'firstName' : firstName,
              'lastName' : lastName,
              'phone' : phone,
              'hashedPassword' : hashedPassword,
              'tosAgreement' : true
            };
            // Store the user
            _data.create('users', phone, userObj, (err)=>{
              if(!err){
                callback(200);
              }else {
                console.log(err);
                callback(500, {'ERROR': 'Could not create new user'});
              }
            });
        }else{
            callback(500, {'ERROR': 'Could not hash password'});
        }
      }else{
        // User already exist
        callback(400, {'ERROR': 'A User with this phone number already exists'});
      }
    });
  } else {
    callback(400,{'ERROR' : 'Missing required fields'});
  }
};

// Users - Get
// Required data: phone
// Optional data: none
// only let the authenticated User access thier object and not anyone's elses
users_handlers._users.get = (data, callback)=>{
  // Check that the phone numner is valid
var phone = typeof(data.queryStringObj.phone) == 'string' && data.queryStringObj.phone.trim().length == 9 ? data.queryStringObj.phone.trim() : false;
  if(phone){
    //Get the token from the token from the headers, the token shold be expected in the request headers
    var token = typeof(data.headers.token) == 'string' ? data.headers.token : false;
    // Verify the token from headers for given phone number
    tokens_handlers.verifyToken(token, phone, (tokenIsValid)=>{
      if(tokenIsValid){
        //Lookup the user
        _data.read('users', phone, (err, resData)=>{
          if(!err && resData ){
            // Removed the hashed password from the user obj before we turning it to the requester
            delete resData.hashedPassword;
            callback(200, resData);
          }else{
            callback(404);
          }
        });
      } else {
        callback(403, {'ERROR': 'The token is not valid or missing'});
      }
    });
  }else{
      callback(400, {'ERROR' : 'Missing phone number'});
  }
};

// Users - Put
// Required data: phone
// Optional data: firstName, lastName, password (at least one must be specified)
// Only let the authenticated User update thier object and not anyone's elses
users_handlers._users.put = (data, callback)=>{
  // Check that the phone numner is valid
  var phone = typeof(data.payload.phone) == 'string' && data.payload.phone.trim().length == 9 ? data.payload.phone.trim() : false;
  var firstName = typeof(data.payload.firstName) == 'string' && data.payload.firstName.trim().length > 0 ? data.payload.firstName.trim() : false;
  var lastName = typeof(data.payload.lastName) == 'string' && data.payload.lastName.trim().length > 0 ? data.payload.lastName.trim() : false;
  var password = typeof(data.payload.password) == 'string' && data.payload.password.trim().length > 0 ? data.payload.password.trim() : false;
  if(phone){
    //Get the token from the token from the headers, the token shold be expected in the request headers
    var token = typeof(data.headers.token) == 'string' ? data.headers.token : false;
    // Verify the token from headers for given phone number
    tokens_handlers.verifyToken(token, phone, (tokenIsValid)=>{
      if(tokenIsValid){
        // Check for the Optional fields
        if(firstName || lastName || password){
          //Lookup the User
          _data.read('users', phone, (err, resData)=>{
            if(!err && resData){
             // Set User data to Update
            resData.firstName = firstName ? firstName : resData.firstName;
            resData.lastName = lastName ? lastName : resData.lastName;
            resData.hashedPassword = password ? helpers.hash(password) : resData.hashedPassword;

              // Update User
              if(helpers.hash(password)){
                _data.update('users', phone, resData, (err)=>{
                  if(!err){
                    callback(200);
                  } else {
                    callback(500, {'ERROR': 'Could not Update User'});
                  }
                });
              } else {
                callback(400, {'ERROR': 'Password is not Hashed'});
              }
          } else {
            callback(400, {'ERROR': 'User with this phone not found'});
          }
          });
        } else{
          callback(400, {'ERROR': 'Missing fields to Update'});
        }
      } else {
        callback(403, {'ERROR': 'The token is not valid or missing'});
      }
    });
  } else {
    callback(400, {'ERROR': 'Missing phone number'});
  }
};

// Users - Delete
// Required data: phone
// @TODO only let the authenticated User Delete thier object and not anyone's elses
// @TODO delete any other data files associated with this User
users_handlers._users.delete = (data, callback)=>{
  // Check that the phone numner is valid
  var phone = typeof(data.queryStringObj.phone) == 'string' && data.queryStringObj.phone.trim().length == 9 ? data.queryStringObj.phone.trim() : false;
  if(phone){
    //Get the token from the token from the headers, the token shold be expected in the request headers
    var token = typeof(data.headers.token) == 'string' ? data.headers.token : false;
    // Verify the token from headers for given phone number
    tokens_handlers.verifyToken(token, phone, (tokenIsValid)=>{
      if(tokenIsValid){
        //Lookup the user
        _data.read('users', phone, (err, resData)=>{
          if(!err && resData ){
            // Delete User
            _data.delete('users', phone, (err)=>{
              if(!err){
                // Delete all Users the check
                var userChecks = typeof(resData.checks) == 'object' && resData.checks instanceof Array ? resData.checks : [];
                var checksToDelete = userChecks.length;
                if(checksToDelete > 0){
                  var checksDeleted = 0;
                  var deletionErrors = false;
                  // Loop through the checks
                  userChecks.forEach((checkId)=>{
                    // Delete the check
                    _data.delete('checks', checkId, (err)=>{
                      if(err){
                        deletionErrors = true;
                      }
                      checksDeleted++;
                      if(checksDeleted == checksToDelete){
                        if(!deletionErrors){
                          callback(200);
                        } else {
                          callback(500, {'ERROR': 'There are errors while delete all checks, some checks may not deleted'});
                        }
                      }
                    });
                  });
                } else {
                  callback(200);
                }
              } else{
                callback(500, {'ERROR': 'Could not delete the User'});
              }
            });
          }else{
            callback(400, {'ERROR': 'Could not find the User with this phone'});
          }
        });
      } else {
        callback(403, {'ERROR': 'The token is not valid or missing'});
      }
    });
  } else{
      callback(400, {'ERROR' : 'Missing phone number'});
  }
};


module.exports = users_handlers;
