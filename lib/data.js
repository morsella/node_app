/*
* Library for storing and editing data
*/

var fs = require('fs');
var path = require('path');
var helpers = require('./helpers');

//Container for the module (to be exported)
var lib= {};

//Base dir of the data folder, __dirname => node const where we are right now
lib.baseDir = path.join(__dirname, '/../.data/');

//Writing data to a file
lib.create = (dir, file, data, callback)=>{
  //Open a file for writing
  fs.open(lib.baseDir + dir + '/' + file + '.json', 'wx', (err, fileDescriptor)=>{
    if(!err && fileDescriptor){
      // Convert data to string
      var stringData  = JSON.stringify(data);
      // Write to file and close the file
      fs.writeFile(fileDescriptor, stringData, (err)=>{
        if(!err){
          fs.close(fileDescriptor, (err)=>{
            if(!err){
              callback(false);
            }else {
              callback('Error closing new file');
            }
          });
        }else {
          callback('Error writing to a new file');
        }
      });
    }else {
      callback('File might be already exist');
    }
  });
};

//Read data from a file
lib.read = (dir, fileName, callback)=>{
  fs.readFile(lib.baseDir + dir + '/' +fileName+ '.json', 'utf-8', (err, data)=>{
    if(!err && data){
      var parsedData = helpers.parseJsonToObject(data);
      callback(false, parsedData);
    }else{
      callback(err, data);
    }
  });
};

//Update data in a file
lib.update = (dir, fileName, data, callback)=>{
  // Open the file for writing
  fs.open(lib.baseDir + dir + '/' + fileName + '.json', 'r+', (err, fileDescriptor)=>{
    if(!err && fileDescriptor){
      var stringData = JSON.stringify(data);
      //Truncate the file
      fs.truncate(fileDescriptor, (err)=>{
        if(!err){
          // Write to the file and close
          fs.writeFile(fileDescriptor, stringData, (err)=>{
            if(!err){
              fs.close(fileDescriptor, (err)=>{
                if(!err){
                  callback(false);
                }else {
                  callback('Error closing the file');
                }
              });
            }else {
              callback('Error writing to an existing file');
            }
          });

        }else {
          callback('Error truncating the file');
        }
      });
    }else {
      callback('Could not open the file for update, might already exist');
    }
  });
};

//Delete file
lib.delete = (dir, fileName, callback)=>{
  //Unlink file
  fs.unlink(lib.baseDir + dir + '/' + fileName + '.json', (err)=>{
    if(!err){
      callback(false);
    }else {
      callback('Error deleting the file');
    }
  });
};


module.exports = lib;
