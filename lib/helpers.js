/*
* Helpers for various tasts
*/
// Dependencies
var querystring = require('querystring');
var https = require('https'); // craft and send https requests
var crypto  = require('crypto');
var config = require('./config');

// Container for all the helpers
var helpers = {};

// takes a string and returns JSON obj from that string or returns false in all cases, without throwing error
// We don't want to parse something that isn't valid JSON
helpers.parseJsonToObject = (str)=>{
  try{
    var obj = JSON.parse(str);
    return obj;
  }catch(e){
    return {};
  }
};

// Create a SHA256 hash
helpers.hash = (str)=>{
  console.log(str);
  if(typeof(str) == 'string' && str.length > 0){
    var hash = crypto.createHmac('sha256', config.hashingSecret).update(str).digest('hex');
    return hash;
  }else {
    return false;
  }
};

helpers.createRandomStr = (strLength)=>{
  strLength = typeof(strLength) == 'number' && strLength > 0 ? strLength : false;
  if(strLength){
    // Define all possible characters that could go into the string
    var possibleChars = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_';

    // Start the final string to create a random string
    var str = '';
    for(i = 1; i <= strLength; i++){
      // Get random character
      var randomChar = possibleChars.charAt(Math.floor(Math.random() * possibleChars.length));
      //Append the character to the final string
      str += randomChar;
    }
    // Return final string
    return str;
  }else {
    return false;
  }
};

// Send an SMS on message via Twilio
helpers.sendTwilioSms = (phone, msg, callback)=>{
    // Validate params
    phone = typeof(phone) == 'string' && phone.trim().length == 9 ? phone.trim() : false;
    msg = typeof(msg) == 'string' && msg.trim().length > 0 && msg.trim().length <= 1600 ? msg.trim() : false;
    if(phone && msg){
      // Configure the request payload for Twilio
      var payload = {
        'From' : config.twilio.fromPhone,
        'To' : '+31'+phone,
        'Body': msg
      };
      // Stringify Payload and configure the request details
      var stringPayload = querystring.stringify(payload);
      // Configure the request details
      // Buffer module is global and available no need to declare it

      var requestDetails={
        'protocol': 'https:',
        'hostname': 'api.twilio.com',
        'method': 'POST',
        'path': '/2010-04-01/Accounts/'+config.twilio.accountSid+'/Messages.json',
        'auth': config.twilio.accountSid+':'+config.twilio.authToken,
        'headers':{
          'Content-Type' : 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(stringPayload)
        }
      };
      // Instantiate the request object
      var req = https.request(requestDetails, (res)=>{
        //Grab the status of the sent request
        var status = res.statusCode;
        // Callback successfully if the request went through
        if(status == 200 || status == 201){
          callback(false); //there was no error
        } else {
          callback('Status Code return was '+status);
        }
      });

      // Bind to an ERROR event in case of Error
      req.on('error', (e)=>{
        callback(e);
      });
      // Add the payload
      req.write(stringPayload);
      // End the req => Send the reqest off
      req.end();
    } else {
      callback('Missing parameters or invalid');
    }
};


module.exports = helpers;
