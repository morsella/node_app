/*
* Create and export configuration variables
*/
//Container for all the environments
var environments = {};

//Setting up default staging object
environments.staging = {
  'httpPort' : 3000,
  'httpsPort': 3001,
  'envName': 'staging',
  'hashingSecret': 'thisIsASecretSalt',
  'maxChecks': 5,
  'twilio' : {
  'accountSid' : 'ACb32d411ad7fe886aac54c665d25e5c5d',
  'authToken' : '9455e3eb3109edc12e3d8c92768f7a67',
  'fromPhone' : '+15005550006'
  }
  // 'twilio':{
  //   'accountSid' : 'ACdd36a641f6da9159623dbf1c2f189e1a',
  //   'authToken' : '23a1dd3936dc6efe5e5c2d39cd16d15c',
  //   'fromPhone' : '+3197004499328'
  // }
};

environments.production = {
  'httpPort' : process.env.PORT || 3000,
  'httpsPort': process.env.PORT || 3001,
  'envName': 'production',
  'hashingSecret': 'thisIsASecretSalt',
  'maxChecks': 5,
  'twilio' : {
    'accountSid' : 'ACb32d411ad7fe886aac54c665d25e5c5d',
    'authToken' : '9455e3eb3109edc12e3d8c92768f7a67',
    'fromPhone' : '+15005550006'
  }
  // 'twilio':{
  //   'accountSid' : 'ACdd36a641f6da9159623dbf1c2f189e1a',
  //   'authToken' : '23a1dd3936dc6efe5e5c2d39cd16d15c',
  //   'fromPhone' : '+3197004499328'
  // }
};

//Which environment was passed as a command-line argument
var currentEnvironment  = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '';

//Check that the current environment is defined above
// If not, default staging env
var environmentToExport = typeof(environments[currentEnvironment]) == 'object' ? environments[currentEnvironment] : environments.staging;

//Export module
module.exports = environmentToExport;
