/*
* Primary file for the API
*/

//Dependencies
var http = require('http');
var https = require('https');
var fs = require('fs');
var url = require('url');
var stringDecoder = require('string_decoder').StringDecoder;
var config = require('./lib/config');
// var insertUser = require('./.data/db/connection');
var handlers = require('./lib/handlers/handlers');
var helpers = require('./lib/helpers');

// var _data = require('./lib/data');
//Test
// _data.delete('test', 'newFile', (err)=>{
//   console.log('Respond: ', err);
// });

//Insert User in DB
// var credentials = { username: "SystemAdmin", pwd: "root" };
// insertUser(credentials, {fistName: "User5", lastName: "User5"});

// @TODO Get rid of this later
helpers.sendTwilioSms('614721702', 'Hello', (err)=>{
  console.log(err);
});
//Create HTTPS Server Options
var httpsServerOptions = {
  //asinc version of the fs function
  //'key' : fs.readFileSync('./https/key.pem'),
  //'cert' : fs.readFileSync('./https/cert.pem')
};
//Instatiate HTTPS Server
var httpsServer = https.createServer(httpsServerOptions,(req, res) => {
  unifiedServer(req, res);
});
//Start HTTPS Server
httpsServer.listen(config.httpsPort, ()=>{
  console.log("Server listens on: " + config.httpsPort);
  console.log("ENV: " + config.envName);
});
//Instatiate HTTP Server
var httpServer = http.createServer((req, res) => {
  unifiedServer(req, res);
});
//Start HTTP Server
httpServer.listen(config.httpPort, ()=>{
  console.log("Server listens on: " + config.httpPort);
  console.log("ENV: " + config.envName);
});
//All the server logic for both http and https
var unifiedServer = (req, res)=>{
  // Get Url and parse it
  // request Url are new everytime when request comes in localhost:3000 by the user
  // true means parse the query string module itself
  var parsedUrl = url.parse(req.url, true);
  // Get the path
  // Untrimed path
  var path = parsedUrl.pathname;
  //trimmed all slashes from the path
  var trimmedPath = path.replace(/^\/+|\/+$/g,'');
  // Get the query string as an object
  // when the user sends url with query parameters,
  // these will be parsed and put inside the obj with keys and values
  var queryStringObj = parsedUrl.query;
  // Get the http method
  var method  = req.method.toLowerCase();
  // Get the headers as an object
  var headers = req.headers;
  // Get the payload, if any
  // bits of information that come in at a time
  // payload come in as part http request to the http server as a string
  // collect this string to figre out what that payload is
  // we need to know what this entire payload is once it's finished streaming
  var decoder = new stringDecoder('utf-8');
  var buffer = '';
  req.on('data', (data)=>{
    buffer += decoder.write(data);
  });
  req.on('end', ()=>{
    buffer += decoder.end();
   // Choose the handler
   // if handler not found use the not found handler
   var chosenHandler = typeof(router[trimmedPath]) !== 'undefined' ? router[trimmedPath] : handlers.notFound;
   // Constract the data obj to send to the handlers
   var data = {
     'trimmedPath': trimmedPath,
     'queryStringObj': queryStringObj,
     'method': method,
     'headers': headers,
     'payload': helpers.parseJsonToObject(buffer)
   };
   //Route the request to the handler specified in the router
   chosenHandler(data, (statusCode, payload)=>{
     //Defalt statusCode or defined by the handler
     statusCode = typeof(statusCode) == 'number'? statusCode : 200;

     //Use the payload as empty obj or defined by the handler
     payload = typeof(payload) == 'object' ? payload: {};

     //every payload that is recieved is an obj an we have to convert to a string
     var payloadString = JSON.stringify(payload);

     // return the response
     res.setHeader('Content-Type', 'application/json');
     res.setHeader("Access-Control-Allow-Origin", "*");
     res.setHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
     res.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Authorization, Origin, Accept");
     res.writeHead(statusCode);
     res.end(payloadString);
     //Log the request path
     console.log('Request path: '+ trimmedPath + ' with this method: '+ method);
     console.log('with queries: ', queryStringObj);
     console.log('headers: ', headers);
     console.log('This response: ', statusCode, payloadString);
    });
  });
};

//Define a request router
var router = {
  'sample' : handlers.sample,
  '': handlers.ping,
  //Ping calls back 200 - monitors the app and find out if it's live or not
  'ping' : handlers.ping,
  'users': handlers.users,
  'tokens': handlers.tokens,
  'checks' : handlers.checks
};
